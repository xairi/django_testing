from unittest import skip
from .base import FunctionalTest


class LayoutAndStyling(FunctionalTest):
    def test_layout_and_Styling(self):
        # Edith goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # She notices the inputbox is nicely centered
        inputbox = self.get_item_input_box()
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2, 560, delta=10
        )
